var highlevelDiscriptor = [
    {
        catagory: "Undergraduate",
        "name": "Associate",
        year: 2,
        points: 3
    },
    {
        catagory: "Undergraduate",
        "name": "Bachelor",
        year: 4,
        points: 4
    },
    {
        catagory: "Graduate",
        "name": "Master",
        year: 2,
        points: 5
    },
    {
        catagory: "Graduate",
        "name": "Doctoral",
        year: 5,
        points: 6
    },
    {
        catagory: "Graduate",
        "name": "Profesional",
        year: 7,
        points: 7
    },
    {
        catagory: "None",
        "name": "Default",
        year: 0,
        points: 3
    }
]

exports.checkForHighlevelDiscriptors = (modeledData) => {
    var level;
    for (data of highlevelDiscriptor){
        if (modeledData.AcademicLevel.startsWith(data.name)){
            modeledData.points = data.points;
            return
        }
    }
    modeledData.level = 3;
};