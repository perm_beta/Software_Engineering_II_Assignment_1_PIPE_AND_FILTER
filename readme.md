********************************
*  Eyoel Wendwosen             *
*  ATR/7467/08                 *
*  Section 2                   *
********************************
This project is a simple node app using PIPE AND FILTER architectural pattern, developed for learning purpose. 
It's a simmple salary calculatro application that allows user to calculate salary given JSON file. 

The application does 5 level filteration process,
1. Modeling the Data - Model.js
2. Filtering the data with High level Academic discriptors like(Bachlor, Masters, Associate) - HighLevelDiscriptor.js
3. Secondary level filter which filters the data with thier postion in the organization such as (Inter, Junior, Senior) - SecondaryDiscriptors.js
4. This is the main filteration process in which  that the math is done, using the points collected from the previous filteration process it calculated the salary. - Calculate Salary.js
5. The last one is the one that displayes the salary of the employee with detailed information - DisplaySalaryInfo.js

To run run Main.js using Node CLI
