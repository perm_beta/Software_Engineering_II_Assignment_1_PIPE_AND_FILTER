var salaryScale = [
    {
        category: 1,
        birrPerHour: 75
    },
    {
        category: 2,
        birrPerHour: 100
    },
    {
        category: 3,
        birrPerHour: 150
    },
    {
        category: 4,
        birrPerHour: 250
    },
    {
        category: 5,
        birrPerHour: 400
    },
    
]

exports.calculate =(modeledData)=>{
    for (salaryCategory of salaryScale){
        if (modeledData.category == salaryCategory.category){
            modeledData.salary =  salaryCategory.category * modeledData.points * modeledData.hoursWoked ;
        }
    }
    
}