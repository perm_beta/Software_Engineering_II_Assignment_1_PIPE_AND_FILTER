var input = require("./InputData"); 
var model = require("./ModelData");
var highLevelDiscriptors = require("./HighLevelDiscriptors");
var secondaryLevelDiscriptor = require("./SecondaryDiscriptors");
var calculateSalary = require("./CalculateSalary");
var display = require("./DisplaySalaryInfo");

function main(){
    var rawData, modelData;
    rawData = input.inputData("./Employee.json");
    modelData = model.modelData(rawData);
    highLevelDiscriptors.checkForHighlevelDiscriptors(modelData);
    secondaryLevelDiscriptor.checkForSecondaryDiscriptors(modelData);
    calculateSalary.calculate(modelData);
    display.DisplayBill(modelData);
}
main();