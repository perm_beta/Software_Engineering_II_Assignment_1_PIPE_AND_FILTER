var secondaryDiscriptor = [
    {
        catagory: "1st",
        "name": "Inter",
        points: 3
    },
    {
        catagory: "1st",
        name: "Junior",
        points: 4
    },
    {
        catagory: "2nd",
        name: "Senior",
        points: 5
    },
    {
        catagory: "2nd",
        name: "Project Manager",
        points: 6
    },
    {
        catagory: "3rd",
        name: "Vice President",
        points: 7
    },
    {
        catagory: "3rd",
        name: "CTO",
        points: 10
    }
]

exports.checkForSecondaryDiscriptors = (modeledData) => {
    for (discription of secondaryDiscriptor){
        if(modeledData.level.startsWith(discription.name)){
            modeledData.points += discription.points;
        }
    }   
    
}