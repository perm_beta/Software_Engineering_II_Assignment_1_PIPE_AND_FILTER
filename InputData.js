var fs = require("fs");

exports.inputData = (path) => {
    let fd;
    fd = fs.readFileSync(path);
    return fd.toString();
};
